@extends('dashboard.layouts.dashboard')
@section('main-content')

@include('dashboard.layouts.notification')
<div class="container-fluid"> 
    <!-- Basic Examples -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        All bookings
                    </h2>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover js-basic-example mydatatable">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone Number</th>
                                    <th>Sport</th>
                                    <th>Ground</th>
                                    <th>Date</th>
                                    <th>Slots</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>No.</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone Number</th>
                                    <th>Sport</th>
                                    <th>Ground</th>
                                    <th>Date</th>
                                    <th>Slots</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                            <tbody> 
                                @php
                                    $no = 1;
                                @endphp
                                @foreach ($bookings as $booking)
                                @php
                                    $is_booked = false;
                                @endphp
                                @foreach ($booking->slots as $s)
                                    @php
                                        if($s->booked){
                                            $is_booked = true;
                                        }
                                    @endphp
                                @endforeach
                                <tr style="{{!$is_booked?'background-color:#ff8f8f;color:white;':''}}">
                                    <td>{{$no}}</td>
                                    <td>{{$booking->name}}</td>
                                    <td>{{$booking->email}}</td>
                                    <td>{{$booking->phone_number}}</td>
                                    <td>{{$booking->sport->name}}</td>
                                    <td>{{$booking->pitch->name}}</td>
                                    <th>{{$booking->date}}</th>
                                    <td> 
                                        @foreach ($booking->slots as $slot)
                                            {{$slot->slot->slot}}<br>
                                        @endforeach
                                    </td>
                                    <td>
                                        @if ($is_booked)
                                            <button data-url="{{url('odashboard/cancel',$booking->id)}}" onclick="confirm(this)" class="btn btn-sm btn-danger">Cancel</a>                                            
                                        @else
                                            Cancelled
                                        @endif
                                    </td> 
                                </tr>      
                                @php
                                    $no++;
                                @endphp                              
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('page-scripts')
<script>
function confirm(target){
    swal({
        title: "Confirm",
        text: "Are you sure you want to cancel this booking?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        })
        .then((willDelete) => {
        if (willDelete) {
            swal("Booking will be cancelled!", {
            icon: "success",
            });
            window.location = $(target).data("url");
        } else {
            swal("The booking wont be cancelled!");
        }
    });
}
  

</script>    
@endsection



