@extends('dashboard.layouts.home')
@section('body-class','menubar-left menubar-unfold menubar-light theme-success') 
@section('css')
<link rel="stylesheet" href="{{asset('dashboard/libs/bower/font-awesome/css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{asset('dashboard/libs/bower/material-design-iconic-font/dist/css/material-design-iconic-font.css')}}">
<!-- build:css ../assets/css/app.min.css -->
<link rel="stylesheet" href="{{asset('dashboard/libs/bower/animate.css/animate.min.css')}}">
<link rel="stylesheet" href="{{asset('dashboard/libs/bower/fullcalendar/dist/fullcalendar.min.css')}}">
<link rel="stylesheet" href="{{asset('dashboard/libs/bower/perfect-scrollbar/css/perfect-scrollbar.css')}}">
<link rel="stylesheet" href="{{asset('dashboard/assets/assets/css/bootstrap.css')}}">
<link rel="stylesheet" href="{{asset('dashboard/assets/assets/css/core.css')}}">
<link rel="stylesheet" href="{{asset('dashboard/assets/assets/css/app.css')}}"> 
<link rel="stylesheet" href="{{asset('dashboard/libs/misc/datatables/datatables.min.css')}}">
<link rel="stylesheet" href="{{asset('dashboard//assets/css/styles.css')}}">
<link rel="stylesheet" href="{{asset('dashboard/temp2/css/style.css')}}">
<!-- endbuild -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,500,600,700,800,900,300">		
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.2/css/star-rating.min.css" />
@yield('page-styles')

<script src="{{asset('dashboard/libs/bower/breakpoints.js/dist/breakpoints.min.js')}}"></script>
<script>
    Breakpoints();
</script>
@endsection
@section('content') 
<!-- APP NAVBAR ==========-->
<nav id="app-navbar" class="navbar navbar-inverse navbar-fixed-top success"> 
    <div class="navbar-header">
      <button type="button" id="menubar-toggle-btn" class="navbar-toggle visible-xs-inline-block navbar-toggle-left hamburger hamburger--collapse js-hamburger">
        <span class="sr-only">Toggle navigation</span>
        <span class="hamburger-box"><span class="hamburger-inner"></span></span>
      </button>
  
      <button type="button" class="navbar-toggle navbar-toggle-right collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="zmdi zmdi-hc-lg zmdi-more"></span>
      </button>
  
      <button type="button" class="navbar-toggle navbar-toggle-right collapsed" data-toggle="collapse" data-target="#navbar-search" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="zmdi zmdi-hc-lg zmdi-search"></span>
      </button>
   
    </div><!-- .navbar-header -->
    
    <div class="navbar-container container-fluid">
      <div class="collapse navbar-collapse" id="app-navbar-collapse">
        <ul class="nav navbar-toolbar navbar-toolbar-left navbar-left">
          <li class="hidden-float hidden-menubar-top">
            <a href="javascript:void(0)" role="button" id="menubar-fold-btn" class="hamburger hamburger--arrowalt is-active js-hamburger">
              <span class="hamburger-box"><span class="hamburger-inner"></span></span>
            </a>
          </li>
          <li>
            <h5 class="page-title hidden-menubar-top hidden-float">Dashboard</h5>
          </li>
        </ul>
      </div>
    </div><!-- navbar-container -->
  </nav>
  <!--========== END app navbar -->
  <!-- APP ASIDE ==========-->
<aside id="menubar" class="menubar light" style="margin-top:0px;">       
        <!-- .app-user -->      
       @include('dashboard.layouts.side-menu')
</aside>
<main id="app-main" class="app-main">
    <div class="wrap" style="min-height:550px;padding-top:55px;">
        <section class="app-content">
            @yield('main-content')
        </section>
        <!-- #dash-content -->
    </div>
    <!-- .wrap -->
    <!-- APP FOOTER -->
    <div class="wrap p-t-0">
        <footer class="app-footer">
            <div class="clearfix">
                <div class="copyright pull-left" style="font-size:10px;"> Developed by <a href="http://legendaryits.com">Legendary IT Solutions</a></div>
            </div>
        </footer>
    </div>
    <!-- /#app-footer -->
</main>

<!--========== END app aside -->      
@endsection
@section('js')
<script src="{{asset('dashboard/libs/bower/jquery/dist/jquery.js')}}"></script>
<script src="{{asset('dashboard/libs/bower/jquery-ui/jquery-ui.min.js')}}"></script>
<script src="{{asset('dashboard/libs/bower/jQuery-Storage-API/jquery.storageapi.min.js')}}"></script>
<script src="{{asset('dashboard/libs/bower/bootstrap-sass/assets/javascripts/bootstrap.js')}}"></script>
<script src="{{asset('dashboard/libs/bower/jquery-slimscroll/jquery.slimscroll.js')}}"></script>
<script src="{{asset('dashboard/libs/bower/perfect-scrollbar/js/perfect-scrollbar.jquery.js')}}"></script>
<script src="{{asset('dashboard/libs/bower/PACE/pace.min.js')}}"></script>
<!-- endbuild -->

<!-- build:js ../assets/js/app.min.js -->
<script src="{{asset('dashboard/assets/assets/js/library.js')}}"></script>
<script src="{{asset('dashboard/assets/assets/js/plugins.js')}}"></script>
<script src="{{asset('dashboard/assets/assets/js/plugins.js')}}"></script>
<script src="{{asset('dashboard/assets/assets/js/app.js')}}"></script>
<script src="{{asset('dashboard/libs/misc/datatables/datatables.min.js')}}"></script>
<!-- endbuild -->
<script src="{{asset('dashboard/libs/bower/moment/moment.js')}}"></script>
<script src="{{asset('dashboard/libs/bower/fullcalendar/dist/fullcalendar.min.js')}}"></script>
<script src="{{asset('dashboard/assets/assets/js/fullcalendar.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.2/js/star-rating.min.js"></script>
<script src="https://unpkg.com/jspdf@latest/dist/jspdf.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="{{asset('dashboard/assets/js/scripts.js')}}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
  $('.mydatatable').dataTable();
  
  $("#input-id").rating();
</script>
@yield('page-scripts')
@endsection