<div class="row" style="margin-top:0px;margin-bottom:40px;">
    <div class="col-md-12 text-center" style="">
        <div class="row">
          <div class="col-md-12 text-center">
            <img src="{{asset('img/logo.png')}}" style="height:150px;" alt=""> 
          </div>
        </div>        
    </div>
  </div>
<div class="menubar-scroll">
    <div class="menubar-scroll-inner">
      <ul class="app-menu">
          
        <li class="{{Request::is('bvdashboard')?'active':''}}">
              <a href="{{url('/bvdashboard')}}">
                <i class="menu-icon zmdi zmdi-home zmdi-hc-lg"></i>
                <span class="menu-text">Dashboard</span>
              </a>
        </li>  
        {{-- <li class="{{Request::is('bvdashboard/hotels')?'active':''}}">
          <a href="{{url('/bvdashboard/hotels')}}">
            <i class="menu-icon zmdi zmdi-layers zmdi-hc-lg"></i>
            <span class="menu-text">Hotels</span>
          </a>
        </li>
        <li class="{{Request::is('bvdashboard/packages')?'active':''}}">
          <a href="{{url('/bvdashboard/packages')}}">
            <i class="menu-icon zmdi zmdi-layers zmdi-hc-lg"></i>
            <span class="menu-text">Packages</span>
          </a>
        </li>    --}}

       
       {{--  <li class="{{Request::is('tours')?'active':''}}">
          <a href="{{url('tours')}}">
            <i class="menu-icon zmdi zmdi-file-text zmdi-hc-lg"></i>
            <span class="menu-text">Tours</span>
          </a>
        </li> 
        @if (session('school_type')=="2") 
        <li class="{{Request::is('scholarship')?'active':''}}">
          <a href="{{url('scholarship')}}">
            <i class="menu-icon zmdi zmdi-dns zmdi-hc-lg"></i>
            <span class="menu-text">Scholarship</span>
          </a>
        </li> 
        @endif 
        <li class="{{Request::is('logs')?'active':''}}">
          <a href="{{url('logs')}}">
            <i class="menu-icon zmdi zmdi-search zmdi-hc-lg"></i>
            <span class="menu-text">Logs</span>
          </a>
        </li>
        <li class="has-submenu {{Request::is('users/*')||Request::is('users')?'active open':''}}">
          <a href="javascript:void(0)" class="submenu-toggle">
            <i class="menu-icon zmdi zmdi-view-dashboard zmdi-hc-lg"></i>
            <span class="menu-text">User Management</span>
            <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
          </a>
          <ul class="submenu" style="{{Request::is('users/*')||Request::is('users')?'display:block;':''}}">
            <li class="{{Request::is('users')?'active':''}}"><a href="{{url('users/')}}"><span class="menu-text">All Users</span></a></li> 
          </ul>
        </li>    
        <li class="has-submenu {{Request::is('reports/*')?'active open':''}}">
          <a href="javascript:void(0)" class="submenu-toggle">
            <i class="menu-icon zmdi zmdi-puzzle-piece zmdi-hc-lg"></i>
            <span class="menu-text">Reports</span>
            <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
          </a>
          <ul class="submenu" style="{{Request::is('reports/*')?'display:block;':''}}">
            <li class="{{Request::is('reports/general')?'active':''}}"><a href="{{url('reports/general')}}"><span class="menu-text">General</span></a></li>
            
          </ul>
        </li> --}}
        <li class="menu-separator"><hr></li>
        <li>
          <a href="{{url('/logout')}}">
            <i class="menu-icon zmdi zmdi-power zmdi-hc-lg"></i>
            <span class="menu-text">Logout</span>
          </a>
        </li>     
      </ul><!-- .app-menu -->
    </div><!-- .menubar-scroll-inner -->
  </div><!-- .menubar-scroll -->