<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Osp dashboard</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<meta name="description" content="Admin, Dashboard, Bootstrap" />
	@yield('css')
	
</head>
<body class="@yield('body-class')" style="@yield('background-color')">
    @yield('content')
  
    @yield('js')
</body>