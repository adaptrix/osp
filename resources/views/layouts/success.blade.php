<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>OSP</title>
        <link href="https://fonts.googleapis.com/css?family=Lalezar" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <link rel="stylesheet" href="{{asset('css/animate.css')}}">
        <link rel="stylesheet" href="{{asset('css/j-checkbox.css')}}">
        <link rel="stylesheet" href="{{asset('css/j-radio-button.css')}}">
        <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
        <link rel="stylesheet" href="{{asset('css/style.css')}}">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        </head>
    <body>
        <div class="container-fluid mb-1">
            <div class="row" id="top-nav">
                <div class="col-lg-12 col-md-12 text-center">
                    <img src="{{asset('img/logo.png')}}" id="top-nav-logo" alt="">
                </div>
            </div>
        </div>
        <div class="container col-lg-6 card" id="form-container">
            <form action="{{url('/book')}}" method="post" class="container">
            <div id="form-title" class="row p-2">
                <h3 id="form-title-text">Booking Successful!</h3>
            </div>
            <div id="form-body" class="row p-2" style="padding-top:20px !important;">
                @csrf                    
                    <div id="step1" class="container">
                        <!-- select date step-->
                        <div class="row text-center">
                            <img src="{{asset('img/logo.png')}}" style="margin:auto;" alt="">
                            <p class="text-center" style="margin:auto; font-size:30px; margin-top:50px;">Your booking is now confirmed. The amount to be paid is {{number_format($total_cost)}}!</p>
                            <a href="{{url('/')}}" class="btn btn-primary" style="margin:auto;margin-top:40px;">Home</a>
                        </div>
                    </div> 
            </div> 
            </form>
            <span id="root-url" data-url="{{url('/')}}"></span>
        </div>  
        <div class="container-fluid">
                <div class="row" id="bottom-nav">
                    <div class="col-lg-6 col-md-6 bottom-social-button text-center d-flex justify-content-center">
                        <a href="http://facebook.com" class="bottom-social-link align-self-center" target="_blank">
                            <i class="fab fa-facebook-f bottom-social-icon"></i>
                        </a>
                    </div>
                    <div class="col-lg-6 col-md-6 bottom-social-button text-center d-flex justify-content-center">
                        <a href="http://instagram.com" class="bottom-social-link align-self-center" target="_blank">
                            <i class="fab fa-instagram bottom-social-icon"></i>
                        </a>
                    </div>
                </div>
            </div>
        <script src="{{asset('js/app.js')}}"></script>
        <script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
        <script src="{{asset('js/j-form.js')}}"></script>
    </body>
</html>
