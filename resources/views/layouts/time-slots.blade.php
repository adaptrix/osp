<table id="time-slots-table">
    @foreach ($slots as $slot)
    @php
        $is_booked = App\TimeSlotBooking::where('booked',true)->where('time_slot_id',$slot->id)->where('date',$date)->where('pitch_id',$pitch_id)->count()>0?true:false; 
    @endphp
    <tr style="{{$is_booked?'background-color:#eaeaea;color:grey;':'background-color:white'}}">
        <td id="slot-label-{{$slot->id}}"><span>{{$slot->slot}}</span></td>
        <td>      
            @if (!$is_booked)
            <label class="container-cb cb-table">
                <input class="slots-checkboxes" type="checkbox" name="slots[]" value="{{$slot->id}}" {{$is_booked?'disabled':''}}>
                <span class="checkmark-cb"></span>
            </label>  
            @else
                <span id="booked-button">Booked</span>  
            @endif                          
            
        </td>
    </tr>                             
    @endforeach
</table>   