<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>OSP</title>
        <link href="https://fonts.googleapis.com/css?family=Lalezar" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <link rel="stylesheet" href="{{asset('css/animate.css')}}">
        <link rel="stylesheet" href="{{asset('css/j-checkbox.css')}}">
        <link rel="stylesheet" href="{{asset('css/j-radio-button.css')}}">
        <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
        <link rel="stylesheet" href="{{asset('css/style.css')}}">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        </head>
    <body>
        <div class="container-fluid mb-1">
            <div class="row" id="top-nav">
                <div class="col-lg-12 col-md-12 text-center">
                    <img src="{{asset('img/logo.png')}}" id="top-nav-logo" alt="">
                </div>
            </div>
        </div>
        <div class="container col-lg-6 card" id="form-container">
            <form action="{{url('/book')}}" method="post" class="container">
            <div id="form-title" class="row p-2">
                <h3 id="form-title-text">Booking Information</h3>
            </div>
            <div id="form-body" class="row p-2" style="padding-top:20px !important;">
                @csrf                    
                    <div id="step1" class="container">
                        <!-- select date step-->
                        <div class="row">
                            <div class="col-lg-12 form-group">
                                <img src="{{asset('img/calendar.png')}}" class="icon" alt="">
                                <label for="" class="input-title">Choose date</label>
                                <div class="input-content">
                                    {{-- <input type="date" min="{{Carbon\Carbon::now()->format('Y-m-d')}}" value="{{Carbon\Carbon::now()->format('Y-m-d')}}" name="date" id="date" class="form-control">     --}}
                                    <input type="text" name="date" class="form-control" value="{{Carbon\Carbon::now()->format("Y-m-d")}}" id="select-date" readonly>
                                </div>
                                
                            </div>
                            <div class="col-lg-12 form-group mt-2" id="" >
                                <img src="{{asset('img/ball.png')}}" class="icon" alt="">
                                <label for="" class="input-title">Select sport</label><br>
                                <div class="input-content">
                                    @foreach (App\Sport::all() as $sport)
                                    <label class="container-rb" id="sport-label-{{$sport->id}}">{{$sport->name}} (60 mins) - <span id="sport-cost-{{$sport->id}}" data-cost="{{$sport->price}}">{{number_format($sport->price)}}</span>/-
                                        <input type="radio" name="sport" value="{{$sport->id}}" {{$loop->first?'checked':''}}>
                                        <span class="checkmark-rb"></span>
                                    </label>      
                                    @endforeach
                                    
                                </div>                            
                                
                            </div>
                        </div>
                    </div>
                    <div id="step2" class="hide">
                        <!-- select sport step-->
                        <img src="{{asset('img/pitch.png')}}" class="icon" alt="">
                        <label for="" class="input-title">Select pitch</label>
                        <div class="row input-content">
                            @foreach (App\Pitch::all() as $pitch)
                            <div class="col-lg-4 col-md-4">
                                <img class="pitch-img" id="{{'pitch-img-'.$pitch->id}}" src="{{asset('img/field.png')}}" alt=""><br><br>                           
                                <label class="container-rb" id="pitch-label-{{$pitch->id}}">{{$pitch->name}}
                                    <input type="radio" name="pitch"id="pitch{{$pitch->id}}" value="{{$pitch->id}}">
                                    <span class="checkmark-rb"></span>
                                </label>
                            </div>                             
                            @endforeach

                        </div>
                    </div>
                    <div id="step3" class="hide">
                        <!-- select pitch step-->
                        <img src="{{asset('img/time.png')}}" class="icon" alt="">
                        <label for="" class="input-title">Select time slot</label>
                        <div class="input-content" id="time-slots-table-container">                                         
                        </div>

                    </div>
                    <div id="step4" class="hide"> 
                        <img src="{{asset('img/user.png')}}" class="icon" alt="">
                        <label for="" class="input-title">Contact information</label>
                        <div style="padding-left:28px;">
                            <div class="col-lg-12 form-group">
                                <label for="" class="input-label">Name</label>
                                <input type="text" name="name" id="name" class="form-control" required>
                            </div>
                            <div class="col-lg-12 form-group">
                                <label for="" class="input-label">Email</label>
                                <input type="email" name="email" id="email" class="form-control" required>
                            </div>
                            <div class="col-lg-12 form-group">
                                <label for="" class="input-label">Phone number</label>
                                <input type="number" name="phone_number" id="phone-number" class="form-control" required>
                            </div>                        
                        </div>

                    </div>
                    <div id="step5" class="hide">
                        <!-- select time slots step-->
                        <label for="" class="input-title">Summary</label>
                        <hr>
                        <div class="row" id="summary-page">
                            <div class="col-lg-6 col-md-6">
                                <span>Date:</span><br>
                                <strong><span id="details-date"></span></strong><br><br>
                                <span>Sport:</span><br>
                                <strong><span id="details-sport"></span></strong><br><br>
                                <span>Pitch:</span><br>   
                                <strong><span id="details-pitch"></span></strong><br><br>                     
                                <span>Time slots:</span><br> 
                                <strong id="details-time-slots"></strong><br><br>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <span>Name:</span><br>   
                                <strong><span id="details-name"></span></strong><br><br>                      
                                <span>Email:</span><br>      
                                <strong><span id="details-email"></span></strong><br><br>                   
                                <span>Phone Number:</span><br> 
                                <strong><span id="details-phone-number"></span></strong><br><br>                  
                                <span>Total cost:</span><br> 
                                <strong id="details-total-cost"></strong><br><br>  
                            </div>
                        </div>                 
                    </div> 
            </div>
            <div id="form-footer" class="row">
                <div class="col-lg-12 text-right">                   
                    <button class="btn btn-success hide" type="button" id="previous-button" onclick="displayPrevious()">Previous</button> 
                    <button class="btn btn-success" type="button" id="next-button" onclick="displayNext()">Next</button>
                    <button class="btn btn-success hide" type="submit" id="submit-button">Book now</button>
                </div>
            </div>
            </form>
            <span id="root-url" data-url="{{url('/')}}"></span>
        </div> 
        
        <div class="container-fluid">
                <div class="row" id="bottom-nav">
                    <div class="col-lg-6 col-md-6 bottom-social-button text-center d-flex justify-content-center">
                        <a href="http://facebook.com" class="bottom-social-link align-self-center" target="_blank">
                            <i class="fab fa-facebook-f bottom-social-icon"></i>
                        </a>
                    </div>
                    <div class="col-lg-6 col-md-6 bottom-social-button text-center d-flex justify-content-center">
                        <a href="http://instagram.com" class="bottom-social-link align-self-center" target="_blank">
                            <i class="fab fa-instagram bottom-social-icon"></i>
                        </a>
                    </div>
                </div>
            </div>
        <script src="{{asset('js/app.js')}}"></script>
        <script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
        <script src="{{asset('js/j-form.js')}}"></script>
    </body>
</html>
