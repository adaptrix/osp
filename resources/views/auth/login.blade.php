
@extends('dashboard.layouts.home')
@section('body-class','simple-page')
@section('background-color','background-color:white;')
@section('css')
<link rel="shortcut icon" sizes="196x196" href="{{asset('dashboard/assets/assets/images/logo.png')}}">	
<link rel="stylesheet" href="{{asset('dashboard/libs/bower/font-awesome/css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{asset('dashboard/libs/bower/material-design-iconic-font/dist/css/material-design-iconic-font.min.css')}}">
<link rel="stylesheet" href="{{asset('dashboard/libs/bower/animate.css/animate.min.css')}}">
<link rel="stylesheet" href="{{asset('dashboard/assets/assets/css/bootstrap.css')}}">
<link rel="stylesheet" href="{{asset('dashboard/assets/assets/css/core.css')}}">
<link rel="stylesheet" href="{{asset('dashboard/assets/assets/css/misc-pages.css')}}">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,500,600,700,800,900,300">
@endsection
@section('content')
<div class="simple-page-wrap" style="margin-top:-40px;">
    <div class="simple-page-logo animated swing">
        <img src="{{asset('img/logo.png')}}" alt="" style="height:160px;">
    </div><!-- logo -->
    <div class="simple-page-form animated flipInY" id="login-form" style="border:1px grey solid;">
<h4 class="form-title m-b-xl text-center">Sign In With Your Account</h4>
<form method="POST" action="{{ url('odashboard/login') }}">
    @csrf
    <div class="form-group">
        <input id="sign-in-email" type="text" class="form-control" name="email" placeholder="Email or Username" required autofocus>        
        @if ($errors->has('email'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('email') }}</strong>
        </span>
    @endif
    </div>

    <div class="form-group">
        <input id="sign-in-password" type="password" class="form-control" placeholder="Password" name="password" required>
        @if ($errors->has('password'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('password') }}</strong>
        </span>
    @endif
    </div>
    <div class="form-group m-b-xl">
        <div class="checkbox checkbox-primary">
            <input type="checkbox" id="keep_me_logged_in"/>
            <label for="keep_me_logged_in">Keep me signed in</label>
        </div>
    </div>
    <input type="submit" class="btn btn-success" value="SIGN IN">
</form>
</div><!-- #login-form -->

<div class="simple-page-footer">


</div><!-- .simple-page-footer -->


</div>
@endsection
@section('js')
<script src="{{asset('dashboard/libs/bower/jquery/dist/jquery.js')}}"></script>
<script src="{{asset('dashboard/libs/bower/jquery-ui/jquery-ui.min.js')}}"></script>
<script src="{{asset('dashboard/libs/bower/jQuery-Storage-API/jquery.storageapi.min.js')}}"></script>
<script src="{{asset('dashboard/libs/bower/bootstrap-sass/assets/javascripts/bootstrap.js')}}"></script>
<script src="{{asset('dashboard/libs/bower/jquery-slimscroll/jquery.slimscroll.js')}}"></script>
<script src="{{asset('dashboard/libs/bower/perfect-scrollbar/js/perfect-scrollbar.jquery.js')}}"></script>
<script src="{{asset('dashboard/libs/bower/PACE/pace.min.js')}}"></script>
<!-- endbuild -->

<!-- build:js"assets/js/app.min.js" -->
<script src="{{asset('dashboard/assets/assets/js/library.js')}}"></script>
<script src="{{asset('dashboard/assets/assets/js/plugins.js')}}"></script>
<script src="{{asset('dashboard/assets/assets/js/app.js')}}"></script>
<!-- endbuild -->
<script src="{{asset('dashboard/libs/bower/moment/moment.js')}}"></script>
<script src="{{asset('dashboard/libs/bower/fullcalendar/dist/fullcalendar.min.js')}}"></script>
<script src="{{asset('dashboard/assets/assets/js/fullcalendar.js')}}"></script>
@endsection 