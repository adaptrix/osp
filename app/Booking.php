<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    public function sport()
    {
        return $this->hasOne('App\Sport', 'id', 'sport_id');
    }

    public function pitch()
    {
        return $this->hasOne('App\Pitch', 'id', 'pitch_id');
    }

    public function slots()
    {
        return $this->hasMany('App\TimeSlotBooking', 'booking_id', 'id');
    }

    public function getTotalCostAttribute()
    {
        $count = $this->slots->count();
        $price = $this->sport->price;
        $totalCost = $count * $price;
        return $totalCost;
    }
}
