<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TimeSlotBooking extends Model
{
    public function slot()
    {
        return $this->hasOne('App\TimeSlot', 'id', 'time_slot_id');
    }
}
