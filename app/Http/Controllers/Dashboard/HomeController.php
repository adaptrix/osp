<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Booking;
use App\TimeSlotBooking;

class HomeController extends Controller
{
    public function index()
    {
        $bookings = Booking::orderBy('created_at','desc')->get();
        return view('dashboard.home.index',['bookings'=>$bookings]);
    }

    public function cancel(Request $request)
    {
        $slotBooking = TimeSlotBooking::where('booking_id',$request->id)->get();  
        foreach($slotBooking as $sb){
            $sb->booked=false;
            $sb->save();
        }
        return redirect()->back()->with('message-success','Booking was cancelled successfully');
    }
}
