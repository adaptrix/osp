<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TimeSlot;
use App\TimeSlotBooking;
use App\Booking;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      //  $this->middleware('auth');
    }

    public function getTimeSlots(Request $request)
    { 
        $slots = TimeSlot::all();
        $booked_slots = TimeSlotBooking::where('date',$request->date)->where('pitch_id',$request->pitch_id)->get();
        return view('layouts.time-slots',['slots'=>$slots,'booked_slots'=>$booked_slots,'date'=>$request->date,'pitch_id'=>$request->pitch_id]);
    }

    public function book(Request $request)
    { 
        $booking = new Booking;
        $booking->date = $request->date;
        $booking->name = $request->name;
        $booking->email = $request->email;
        $booking->phone_number = $request->phone_number;
        $booking->sport_id = $request->sport;
        $booking->pitch_id = $request->pitch;
        $booking->save();

        foreach ($request->slots as $key => $value) {
            $tsb = new TimeSlotBooking;
            $tsb->date = $request->date;
            $tsb->booking_id=$booking->id;
            $tsb->time_slot_id = $value;
            $tsb->pitch_id = $request->pitch;
            $tsb->save();            
        }

        return view('layouts.success',["total_cost"=>$booking->total_cost]);
    }
}
