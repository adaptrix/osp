<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Auth::routes();

//login
Route::get('/odashboard/login','Auth\LoginController@showLoginForm')->name('login');
Route::post('/odashboard/login', 'Auth\LoginController@login');

//logout
Route::get('logout',function(){
    Auth::logout();
    return redirect('/odashboard/login');        
});

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/gettimeslots','HomeController@getTimeSlots');
Route::post('/book','HomeController@book');
Route::get('/success', function(){
    $total_cost = 50000;
    return view('layouts.success',['total_cost'=>$total_cost]);
});

Route::prefix('odashboard')->middleware('auth')->namespace('Dashboard')->group(function(){

    Route::get('/','HomeController@index');
    Route::get('/cancel/{id}','HomeController@cancel');
});
