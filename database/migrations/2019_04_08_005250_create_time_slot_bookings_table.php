<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimeSlotBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('time_slot_bookings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('date');
            $table->integer('booking_id');
            $table->integer('time_slot_id');
            $table->integer('pitch_id');
            $table->boolean('booked')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('time_slot_bookings');
    }
}
