<?php

use Illuminate\Database\Seeder;
use App\Sport;

class SportsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sport = new Sport;
        $sport->name = "Football";
        $sport->price = "100000";
        $sport->save();
        $sport = new Sport;
        $sport->name = "Basketball";
        $sport->price = "50000";
        $sport->save();
        $sport = new Sport;
        $sport->name = "Volleyball";
        $sport->price = "50000";
        $sport->save();
        $sport = new Sport;
        $sport->name = "Cricket";
        $sport->price = "80000";
        $sport->save();
    }
}
