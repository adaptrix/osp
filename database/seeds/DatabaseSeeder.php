<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PitchesTableSeeder::class);
        $this->call(SportsTableSeeder::class);
        $this->call(TimeSlotsTableSeeder::class);
    }
}
