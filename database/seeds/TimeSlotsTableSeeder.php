<?php

use Illuminate\Database\Seeder;
use App\TimeSlot;

class TimeSlotsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $slot = new TimeSlot;
        $slot->slot = "7:00 am - 8:00 am";
        $slot->save();
        $slot = new TimeSlot;
        $slot->slot = "8:00 am - 9:00 am";
        $slot->save();
        $slot = new TimeSlot;
        $slot->slot = "9:00 am - 10:00 am";
        $slot->save();
        $slot = new TimeSlot;
        $slot->slot = "10:00 am - 11:00 am";
        $slot->save();
        $slot = new TimeSlot;
        $slot->slot = "11:00 am - 12:00 pm";
        $slot->save(); 
        $slot = new TimeSlot;
        $slot->slot = "12:00 pm - 1:00 pm";
        $slot->save(); 
        $slot = new TimeSlot;
        $slot->slot = "1:00 pm - 2:00 pm";
        $slot->save(); 
        $slot = new TimeSlot;
        $slot->slot = "2:00 pm - 3:00 pm";
        $slot->save(); 
        $slot = new TimeSlot;
        $slot->slot = "3:00 pm - 4:00 pm";
        $slot->save(); 
        $slot = new TimeSlot;
        $slot->slot = "4:00 pm - 5:00 pm";
        $slot->save(); 
        $slot = new TimeSlot;
        $slot->slot = "5:00 pm - 6:00 pm";
        $slot->save(); 
        $slot = new TimeSlot;
        $slot->slot = "6:00 pm - 7:00 pm";
        $slot->save(); 
        $slot = new TimeSlot;
        $slot->slot = "7:00 pm - 8:00 pm";
        $slot->save(); 
        $slot = new TimeSlot;
        $slot->slot = "8:00 pm - 9:00 pm";
        $slot->save(); 
    }
}
