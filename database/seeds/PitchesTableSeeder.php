<?php

use Illuminate\Database\Seeder;
use App\Pitch;

class PitchesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pitch = new Pitch;
        $pitch->name = "Astro Turf Football Field 1";
        $pitch->save();
        
        $pitch = new Pitch;
        $pitch->name = "Astro Turf Football Field 2";
        $pitch->save();

        $pitch = new Pitch;
        $pitch->name = "Multi-purpose";
        $pitch->save();
    }
}
