
function cropImage(target, name, width = 1, height =1){ 
  //create the modal first, if it doesnt exist 
 // var imageEditModal = $();
 // console.log(imageEditModal);
  if(!document.getElementById('#image-crop-modal-'+name)){
    $('body').prepend(`
    <div id="image-crop-modal-${name}" class="modal fade" role="dialog">
        <div class="modal-dialog"> 
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Crop Image</h4>
                </div>
                <div class="modal-body text-center"> 
                    <div class="row">
                        <div class="col-lg 12">
                            <div style="height:400px;" class="text-center" id="canvas-container-${name}" >
                                <canvas id="canvas-cropper-${name}">
                                    Your browser does not support the HTML5 canvas element.
                                </canvas>  
                            </div>
                        </div>
                    </div> 
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="crop-button-${name}">Crop</button>
                    <button type="button" class="btn btn-default" id="close-button-${name}" data-dismiss="modal">Close</button>
                </div>
            </div> 
        </div>
    </div>
    `);
  } 
  var empty_canvas = $(`#canvas-container-${name}`).html(), img, cropper, canvas, context, 
  imageEditModal = $(`#image-crop-modal-${name}`);
    if (target.files && target.files[0]) {
      if ( target.files[0].type.match(/^image\//) ) { 
        if(cropper){
            cropper.destroy();             
            context.clearRect(0,0,canvas.width,canvas.height);
        }
        imageEditModal.off('shown.bs.modal');
        imageEditModal.off('hidden.bs.modal');
        $('#crop-button-'+ name).off('click'); 
        cropper = null;
        img = null; 
        canvas  = document.getElementById('canvas-cropper-'+name);
        console.log(canvas);
        context = canvas.getContext("2d");

        var reader = new FileReader();
        reader.onload = function(evt) {
           img = new Image();
           img.onload = function() {
            imageEditModal.modal(); 
            context.canvas.height = img.height;
            context.canvas.width  = img.width;
            context.drawImage(img, 0, 0);
            cropper = new Cropper(canvas,{  
                aspectRatio: width/height,// 462 / 544,
                minContainerWidth : 350,
                minContainerHeight : 350,
                viewMode : 1
            });    
            imageEditModal.on('shown.bs.modal', function () {
                $('#crop-button-'+name).off('click'); 
                $('#crop-button-'+name).on('click',function() { 
                    console.log('crop clicked ', cropper);
                    // Get a string base 64 data url
                    var canvass = cropper.getCroppedCanvas();
                    var croppedImageDataURL = canvass.toDataURL("image/png"); 
                    // console.log(croppedImageDataURL);
                    $('#result-'+name).html( $('<img>').attr('src', croppedImageDataURL) );
                   // var img = document.createElement('img');
                   // img.src = croppedImageDataURL;
                    var result = document.getElementById(`result-${name}`);
                    
                   // result.innerHTML = img.innerHTML;

                    imageEditModal.modal('hide');
                    $('#image-string-'+name).val(croppedImageDataURL);
                    result.addEventListener('click',function(){
                        imageEditModal.modal('show');
                    });
                }); 
                imageEditModal.on('hidden.bs.modal',function(){ 
                });
            });            
           };
           img.src = evt.target.result; 
		};
        reader.readAsDataURL(target.files[0]);
      }
      else {
        alert("Invalid file type! Please select an image file.");
      }
    }
    else {
      alert('No file(s) selected.');
    }
} 