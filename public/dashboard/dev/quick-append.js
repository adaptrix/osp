var divData = new Object();
var editorCount = 1;
$('.q-append').each(function(){
    divData[$(this).data("name")] = this;
    $(this).append(` 
        <button type="button" class="btn btn-sm btn-primary q-append-button" onclick="replicate(this)">+</button>
    `);
});  
function replicate(target){ 
    var parentTarget = $(target).parent();
    var parentData = $(divData[parentTarget.data("name")]);
    var targetClone = parentData.clone(); 
    console.log(target);
    targetClone.find('.q-append-button').replaceWith(`
        <button type="button" class="btn btn-sm btn-danger" onclick="remove(this)">-</button>
    `); 
    var newDiv = ` 
        <div class="row">  
            ${targetClone.html()}
        </div>
    `; 
    $($(target).parent()).parent().append(newDiv); 
    try{ 
        var tec = 1;
        $('.ice').each(function(){
            $(this).attr('id','ice'+tec);
            try{ 
                CKEDITOR.replace('ice'+tec);
            } catch (error) {
                console.log(error);
            }
            tec++;
        }); 
    }catch(error){
        console.log(error);
    }    
}

function remove(target){
    $(target).parent().remove();
}