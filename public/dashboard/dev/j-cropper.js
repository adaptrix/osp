var cropperCount = new Object();
$('.cropImage').find('img').each(function () {
    var name = new Date().getTime().toString();
    var cropperName = $(this).parent().data("name");
    var urlString = toDataUrl(this); 

    cropperCount[cropperName] = cropperCount[cropperName]?cropperCount[cropperName]+1:1; 

    cropImageDiv = `
                <div>
                    <input type="hidden" id="input-${name}" value="${urlString}" name="${cropperName}-image-string[]"/>
                    <div class="cropImageContainer cropImageExisting"> 
                        <div class="cropImageRemove" onclick="deleteExisting(this)">
                            <p class="cropImageRemoveText">
                                Remove
                            </p>
                        </div> 
                        <img src="${urlString}" id="result-${name}" class="cropImage" onclick="cropExisting(this)"/>
                    </div>
                </div>`;
    $(this).replaceWith(cropImageDiv); 
});

function deleteExisting(deleteExistingTarget){
    deleteImageContainer(deleteExistingTarget);
}

$('.cropImage').each(function(){    
    var cropperName = $(this).data("name");
    var imageCount = parseInt($(this).data("imageCount")); 
    cropperCount[cropperName] = cropperCount[cropperName]?cropperCount[cropperName]:0;
    if(cropperCount[cropperName]<imageCount){ 
        $(this).append(`   
            <div class="addImageContainer">
                <input type="file" class="cropImageFileOpen" id="cropImageFileOpen" style="display:none"
                onchange="startCropping(this)"/>
                <div class="cropImageContainer addImage" id="addNewImage" onclick="selectImage(this)"> 
                    <h4>Add image..</h4>
                </div>
            </div>
        `); 
    }else{    
        $(this).append(`   
            <div class="addImageContainer" style="display:none">
                <input type="file" class="cropImageFileOpen" id="cropImageFileOpen" style="display:none"
                onchange="startCropping(this)"/>
                <div class="cropImageContainer addImage" id="addNewImage" onclick="selectImage(this)"> 
                    <h4>Add image..</h4>
                </div>
            </div>
        `);    
    }
});

function selectImage(target){
    $($(target).parent()).find('.cropImageFileOpen').click(); 
}

function startCropping(target){
    var name = new Date().getTime().toString();
    cropImage(target, name);
}

$('.cropImage').fadeIn();

$('.cropImageRemove').on('click', function () {
    deleteImageContainer(this);
}); 

function cropImage(target, name) { 
    if (!document.getElementById('#image-crop-modal-' + name)) {
        $('body').prepend(`
      <div id="image-crop-modal-${name}" class="modal fade" role="dialog">
          <div class="modal-dialog"> 
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Crop Image</h4>
                  </div>
                  <div class="modal-body text-center"> 
                      <div class="row">
                          <div class="col-lg 12">
                              <div style="height:400px;" class="text-center" id="canvas-container-${name}" >
                                  <canvas id="canvas-cropper-${name}">
                                      Your browser does not support the HTML5 canvas element.
                                  </canvas>  
                              </div>
                          </div>
                      </div> 
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-primary" id="crop-button-${name}">Crop</button>
                      <button type="button" class="btn btn-default" id="close-button-${name}" data-dismiss="modal">Close</button>
                  </div>
              </div> 
          </div>
      </div>
      `);
    }
    var empty_canvas = $(`#canvas-container-${name}`).html(),
        img, cropper, canvas, context,
        imageEditModal = $(`#image-crop-modal-${name}`);
    var size = parseFloat($($(target).parent()).parent().data("cropWidth"))/parseFloat($($(target).parent()).parent().data("cropHeight"));
    console.log(size);
    if (target.files && target.files[0]) {
        if (target.files[0].type.match(/^image\//)) {
            if (cropper) {
                cropper.destroy();
                context.clearRect(0, 0, canvas.width, canvas.height);
            }
            imageEditModal.off('shown.bs.modal');
            imageEditModal.off('hidden.bs.modal');
            $('#crop-button-' + name).off('click');
            cropper = null;
            img = null;
            canvas = document.getElementById('canvas-cropper-' + name);
            console.log(canvas);
            context = canvas.getContext("2d");

            var reader = new FileReader();
            reader.onload = function (evt) {
                img = new Image();
                img.onload = function () {
                    imageEditModal.modal();
                    context.canvas.height = img.height;
                    context.canvas.width = img.width;
                    context.drawImage(img, 0, 0);
                    cropper = new Cropper(canvas, {
                        aspectRatio: size, //width / height, // 462 / 544,
                        minContainerWidth: 350,
                        minContainerHeight: 350,
                        viewMode: 1
                    });
                    imageEditModal.on('shown.bs.modal', function () {
                        $('#crop-button-' + name).off('click');
                        $('#crop-button-' + name).on('click', function () {
                            console.log('crop clicked ', cropper);
                            var canvass = cropper.getCroppedCanvas();
                            var croppedImageDataURL = canvass.toDataURL("image/png");
                            addImageThumbnail(croppedImageDataURL, name, target);
                            var result = document.getElementById(`result-${name}`);
                            imageEditModal.modal('hide');
                            result.addEventListener('click', function () {
                                imageEditModal.modal('show');
                            });
                        });
                        imageEditModal.on('hidden.bs.modal', function () {});
                    });
                };
                img.src = evt.target.result;
            };
            reader.readAsDataURL(target.files[0]);
        } else {
            alert("Invalid file type! Please select an image file.");
        }
    } else {
        alert('No file(s) selected.');
    }
}

function deleteImageContainer(thisTarget) {
    var deleteTarget = $(thisTarget).parent();
    var addImageContainer = $($(deleteTarget).parent()).parent().find(".addImageContainer");
    var cropperName = $($(deleteTarget).parent()).parent().data("name");
    var imageCount = parseInt($($(deleteTarget).parent()).parent().data("imageCount"));

    cropperCount[cropperName] = cropperCount[cropperName]?cropperCount[cropperName]-1:1;
    console.log(imageCount,cropperCount);
    if(cropperCount[cropperName]>imageCount){
        addImageContainer.fadeOut();
    }else{
        addImageContainer.fadeIn();        
    }

    $(deleteTarget).parent().remove();
}

function addImageThumbnail(urlString, name, target) {
    var cropperName = $($(target).parent()).parent().data("name");
    var imageCount = parseInt($($(target).parent()).parent().data("imageCount"));

    cropperCount[cropperName] = cropperCount[cropperName]?cropperCount[cropperName]+1:1;
    if(cropperCount[cropperName]>=imageCount){
        $(target).parent().fadeOut();
    }else{
        $(target).parent().fadeIn();        
    }
    
    if (!$(`#result-${name}`).length) { 
        $(` 
                    <div>
                        <input type="hidden" id="input-${name}" value="${urlString}" name="${cropperName}-image-string[]"/>
                        <div class="cropImageContainer"> 
                            <div class="cropImageRemove">
                                <p class="cropImageRemoveText">
                                    Remove
                                </p>
                            </div> 
                            <img src="${urlString}" class="cropImage" id="result-${name}"/>
                        </div>
                    </div>
        `).insertBefore($(target).parent());
        $('.cropImageRemove').on('click', function () {
            deleteImageContainer(this);
        });
    } else {
        console.log('replacing');
        $(`#result-${name}`).attr('src', urlString);
        $(`#input-${name}`).val(urlString);
    }
}

// handling existing image.
function cropExisting(cropExistingTarget) {
    var targetImage = $(cropExistingTarget);
    var name = targetImage.attr('id').replace('result-', '');
    if ($(`#image-crop-modal-${name}`).length<1) {
        console.log('in');
        var height = 1;
        var width = 1;
        if (!document.getElementById('#image-crop-modal-' + name)) {
            $('body').prepend(`
      <div id="image-crop-modal-${name}" class="modal fade" role="dialog">
          <div class="modal-dialog"> 
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Crop Image</h4>
                  </div>
                  <div class="modal-body text-center"> 
                      <div class="row">
                          <div class="col-lg 12">
                              <div style="height:400px;" class="text-center" id="canvas-container-${name}" >
                                  <canvas id="canvas-cropper-${name}">
                                      Your browser does not support the HTML5 canvas element.
                                  </canvas>  
                              </div>
                          </div>
                      </div> 
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-primary" id="crop-button-${name}">Crop</button>
                      <button type="button" class="btn btn-default" id="close-button-${name}" data-dismiss="modal">Close</button>
                  </div>
              </div> 
          </div>
      </div>
      `);
        }
        var empty_canvas = $(`#canvas-container-${name}`).html(),
            img, cropper, canvas, context,
            imageEditModal = $(`#image-crop-modal-${name}`);
        if (cropper) {
            cropper.destroy();
            context.clearRect(0, 0, canvas.width, canvas.height);
        }
        imageEditModal.off('shown.bs.modal');
        imageEditModal.off('hidden.bs.modal');
        $('#crop-button-' + name).off('click');
        cropper = null;
        img = null;
        canvas = document.getElementById('canvas-cropper-' + name);
        context = canvas.getContext("2d");
        img = new Image();
        var size = parseFloat($($(cropExistingTarget).parent()).parent().data("cropWidth"))/parseFloat($($(this).parent()).parent().data("cropHeight"));
        img.onload = function () {
            imageEditModal.modal();
            context.canvas.height = img.height;
            context.canvas.width = img.width;
            context.drawImage(img, 0, 0);
            cropper = new Cropper(canvas, {
                aspectRatio: size, // 462 / 544,
                minContainerWidth: 350,
                minContainerHeight: 350,
                viewMode: 1
            });
            imageEditModal.on('shown.bs.modal', function () {
                $('#crop-button-' + name).off('click');
                $('#crop-button-' + name).on('click', function () {
                    var canvass = cropper.getCroppedCanvas();
                    var croppedImageDataURL = canvass.toDataURL("image/png");
                    addImageThumbnail(croppedImageDataURL, name);
                    var result = document.getElementById(`result-${name}`);
                    imageEditModal.modal('hide');
                    result.addEventListener('click', function () {
                        imageEditModal.modal('show');
                    });
                });
                imageEditModal.on('hidden.bs.modal', function () {});
            });
        };
        img.src = targetImage.attr('src');
    }
    else{
        $(`#image-crop-modal-${name}`).modal('show');
    }
}

function toDataUrl(image) { 
    var c = document.createElement('canvas'); 
    var img = image; //document.getElementById(id);
    c.height = img.naturalHeight;
    c.width = img.naturalWidth;
    var ctx = c.getContext('2d');

    ctx.drawImage(img, 0, 0, c.width, c.height);
    return base64String = c.toDataURL();
}