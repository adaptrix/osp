
function printDiv(divName){
    console.log(divName);
    var printContents = document.getElementById(divName).innerHTML;
    var buttonsDiv = $('#print_buttons');
    var printButton = $('#print_button');
    var downloadButton = $('#download_button');

    var storePrintButtton = printButton;
    var storeDownloadButton = downloadButton;

    printButton.remove();
    downloadButton.remove();

    var originalContents = document.body.innerHTML;
  //  document.body.innerHTML = printContents;
    window.print();
  //  document.body.innerHTML = originalContents;
     storePrintButtton.appendTo(buttonsDiv);
     storeDownloadButton.appendTo(buttonsDiv);
}

var doc = new jsPDF();
var specialElementHandlers = {
    '#editor': function (element, renderer) {
        return true;
    }
};

function downloadPdf(){
    doc.fromHTML($('#info').html(), 15, 15, {
        'width': 170,
            'elementHandlers': specialElementHandlers
    });
    doc.save('sample-file.pdf'); 
}

$('#dob').on('change',function(){
    var birthDate = $('#dob').val();
    console.trace(birthDate);
    var age = getAge(birthDate);
    $('#years').val(age.years);
    $('#months').val(age.months);
})

function getAge(birthDate, ageAtDate) {
    var daysInMonth = 30.436875; // Days in a month on average.
    var dob = new Date(birthDate);
    var aad;
    if (!ageAtDate) aad = new Date();
    else aad = new Date(ageAtDate);
    if((dob-aad)<0){
        var yearAad = aad.getFullYear();
        var yearDob = dob.getFullYear();
        var years = yearAad - yearDob; // Get age in years.
        dob.setFullYear(yearAad); // Set birthday for this year.
        var aadMillis = aad.getTime();
        var dobMillis = dob.getTime();
        if (aadMillis < dobMillis) {
            --years;
            dob.setFullYear(yearAad - 1); // Set to previous year's birthday
            dobMillis = dob.getTime();
        }
        var days = (aadMillis - dobMillis) / 86400000;  
        var monthsDec = days / daysInMonth; // Months with remainder.
        var months = Math.floor(monthsDec); // Remove fraction from month.
        days = Math.floor(daysInMonth * (monthsDec - months)); 
    }
    else{
        var years = 0;
        var days = 0;
        var months = 0;
    }
    
    return {years: years, months: months, days: days};
}

function admit(id){
    swal("Continue with admission?", {
        buttons: {
          cancel: "Cancel",
          catch: {
            text: "Continue",
            value: "catch",
          }, 
        },
      })
      .then((value) => {
        switch (value) { 
          case "catch":
            continueAdmitting(id);
            break;
       
          default:
            swal("Cancelled!");
        }
      });
}

function continueAdmitting(id){
    var url = $('#admission_url').val() + '/' + id;
    window.location.href = url;
}