var nextStep = 2;
var currentStep = 1;
var previousStep = 0;
var ANIMATION_NEXT_IN = "fadeIn";
var ANIMATION_NEXT_OUT = "fadeOut";
var ANIMATION_PREVIOUS_OUT = "fadeOut";
var ANIMATION_PREVIOUS_IN = "fadeIn";
var STEP_CONTAINER_NAME = "#step";
var LAST_STEP = 5;

var PREVIOUS_BUTTON = "#previous-button";
var NEXT_BUTTON = "#next-button";
var SUBMIT_BUTTON = "#submit-button";

var TIME_SLOTS_TABLE_CONTAINER = "#time-slots-table-container";
var ROOT_URL = $('#root-url').data('url'); 


$(document).ready(function(){
    //alert('we in');
    $('#select-date').datepicker({
        minDate: new Date(),
        dateFormat: 'yy-mm-dd'
    });
});

function animateElemIn(element, animation){
    $(element).attr('class',`animated ${animation} container`);
}

function animateElemOut(element, animation){
    $(element).attr('class',`animated ${animation} hide`);
}

function displayNext(){ 
    if(loadNextPage()){
        animateElemOut(STEP_CONTAINER_NAME+currentStep,ANIMATION_PREVIOUS_OUT);
        animateElemIn(STEP_CONTAINER_NAME+nextStep, ANIMATION_NEXT_IN);

        if(currentStep < LAST_STEP){
            currentStep++;
            previousStep++;
            nextStep++;
        }

        if(currentStep <= 1){
            $(PREVIOUS_BUTTON).hide();
        }else if(currentStep>1){ 
            $(PREVIOUS_BUTTON).show();
        }

        if(currentStep === LAST_STEP){
            $(NEXT_BUTTON).hide();
            $(SUBMIT_BUTTON).show();
        }else{
            $(NEXT_BUTTON).show();
            $(SUBMIT_BUTTON).hide();
        }     
    }
    
}

function displayPrevious(){
    if(currentStep>1){
        currentStep--;
        previousStep--;
        nextStep--;
    }
    if(currentStep>1){
        $(PREVIOUS_BUTTON).show();
    }else{
        $(PREVIOUS_BUTTON).hide();
    }
    animateElemOut(STEP_CONTAINER_NAME+nextStep,ANIMATION_NEXT_OUT);
    animateElemIn(STEP_CONTAINER_NAME+currentStep, ANIMATION_PREVIOUS_IN); 
    
    if(currentStep === LAST_STEP){
        $(NEXT_BUTTON).hide();
        $(SUBMIT_BUTTON).show();
    }else{
        $(NEXT_BUTTON).show();
        $(SUBMIT_BUTTON).hide();
    } 
}

function loadNextPage(){
    switch(nextStep){
        case 2: 
             return loadPitches();
            break;
        case 3: 
            return loadTimeSlots();
            break;  
        case 4: 
            return loadContactDetails();
            break;  
        case 5:
            return loadBookingInformation(); 
            break;
    }
}

function loadPitches(){
    if(dateConvert($('#select-date').datepicker('getDate'))!=null){        
        var sport = $("input[name='sport']:checked").val();
        if(parseInt(sport) === 1){
            $('#pitch1').attr('disabled',false);
            $('#pitch2').attr('disabled',false);
            $('#pitch3').attr('disabled',false);

            $('#pitch1').prop('checked', true);
            $('#pitch3').prop('checked', false);
            
            $('#pitch-img-1').attr('class','pitch-img');
            $('#pitch-img-2').attr('class','pitch-img');
            $('#pitch-img-3').attr('class','pitch-img');
        }else{        
            $('#pitch1').attr('disabled',true);
            $('#pitch2').attr('disabled',true);
            $('#pitch3').attr('disabled',false);
            
            $('#pitch1').prop('checked', false);
            $('#pitch2').prop('checked', false);
            $('#pitch3').prop('checked', true);
            
            $('#pitch-img-1').attr('class','pitch-img-faded');
            $('#pitch-img-2').attr('class','pitch-img-faded');
            $('#pitch-img-3').attr('class','pitch-img');
        }
        return true;
    }else{
        alert('Please select a date first');
    }
}

function loadTimeSlots(){
    var pitchId = $("input[name='pitch']:checked").val();
    var date = dateConvert($('#select-date').datepicker('getDate')); 
    var url = ROOT_URL+'/gettimeslots?pitch_id='+pitchId+'&date='+date;
    $(TIME_SLOTS_TABLE_CONTAINER).html(""); 

    $.get(
        url,
        function(data,status) {
            $(TIME_SLOTS_TABLE_CONTAINER).html(data);
        }
    )
    return true;
}

function loadContactDetails() {
    if ($("input:checkbox:checked").length > 0)
    {
        return true;
    }
    else
    { 
        alert('Please select atleast one time slot');
        return false;
    }
}

function loadBookingInformation(){
    var selectedSportId = $("input[name='sport']:checked").val();
    var selectedPitchId = $("input[name='pitch']:checked").val();
    
    var detailDate = dateConvert($('#select-date').datepicker('getDate'));
    var detailSport = $('#sport-label-'+selectedSportId).text();
    var detailPitch = $('#pitch-label-'+selectedPitchId).text();
    var detailName = $("input[name='name']").val();
    var detailEmail = $("input[name='email']").val();
    var detailPhoneNumber = $("input[name='phone_number']").val();
    var detailTotalCost = parseInt($("#sport-cost-"+selectedSportId).data("cost"));
    
    if(!detailName){
        alert("A name is required");
        return false;
    }
    if(!detailEmail){
        alert("An email is required");
        return false;
    }
    if(!detailPhoneNumber){
        alert("A phone number is required");
        return false;
    }

    var slotsText = "";
    var totalCost = 0;
    $.each($(".slots-checkboxes:checked"), function(){      
        var selectedSlotId = $(this).val(); 
        var slot = $('#slot-label-'+selectedSlotId).text(); 
        slotsText += '<span>'+slot+'</span><br>';
        totalCost = detailTotalCost + totalCost;
    }); 
    $('#details-name').text(detailName);
    $('#details-email').text(detailEmail);
    $('#details-phone-number').text(detailPhoneNumber);
    $('#details-date').text(detailDate);
    $('#details-sport').text(detailSport);
    $('#details-pitch').text(detailPitch);
    $('#details-time-slots').html(slotsText);
    $('#details-total-cost').text(totalCost.toLocaleString() + "/=");
    return true;
} 

function dateConvert(originaldate) { 
    var date = new Date(originaldate);
    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();
    if (day < 10) {
        day = "0" + day;
    }
    if (month < 10) {
        month = "0" + month;
    }
    // var date =  month + "/" + day + "/" + year; 
    var date = year + "-" + month + "-" + day;
    return date; 
}